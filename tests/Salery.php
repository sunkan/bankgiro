<?php 
include '../src/Salary.php';

class BankGiroSaleryTest extends PHPUnit_Framework_TestCase
{

    /**
     * @expectedException PHPUnit_Framework_Error
     * @expectedExceptionMessageRegExp /DateTimeInterface/
     */
     public function testSetDate() {
        $salary = new BankGiro\Salary(123456, 654321);
        $date = new DateTime('2014-10-25');
        $salary->setDate($date);
        $this->assertInstanceOf('DateTimeInterface', $salary->getDate());
        $this->assertEquals($date, $salary->getDate());
        $salary->setDate();
    }

    public function testFormat() {
        $date = new DateTime('2014-10-25');
        $salary = new BankGiro\Salary(123456, 654321, $date);

        $salary->add('3300-8301011698', 1234, 1, 'Pay for.me');
        $salary->add('8381-6125873687', 4321, 2, 'Pay for.me');



        $string = $salary->format();
        $rows = explode("\r\n", $string);
        $this->assertEquals('', $rows[count($rows)-1]);
        $this->assertRegExp('/01(\d{6})\s{2}LÖN\s{46}([A-Z]{3})(\d{6})(\d{10})\s{2}/', $rows[0]);

        $this->assertEquals(80, strlen($rows[1]));
        $this->assertRegExp('/35(\d{6})\s{4}\d{16}\d{12}\s{18}\d{10}(.*){12}/', $rows[1]);
        
        $this->assertEquals(80, strlen($rows[2]));
        $this->assertRegExp('/35(\d{6})\s{4}\d{16}\d{12}\s{18}\d{10}(.*){12}/', $rows[2]);
        

        $this->assertRegExp('/09(\d{6})\s{20}(\d{12})(\d{6})0{34}/', $rows[count($rows)-2]);
        
    }
}