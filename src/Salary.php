<?php

namespace BankGiro;

use DateTimeInterface;

/**
* Class to build BankGiro Salary format
*
* @package BankGiro
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
class Salary {
    /**
     * Array containing each salary 
     * @var array
     */
    private $rows = [];

    /**
     * Array of the diffrent row formats
     * @var array
     */
    private $formats = [
        'TK01'=>"01%d  LÖN                                              %3s%06d%010d  \r\n",
        'TK35'=>"35%d    %d%012d%012d                  %010d%-12s\r\n",
        'TK09'=>"09%d                    %012d%06d0000000000000000000000000000000000\r\n"
    ];

    /**
     * Your account at BankGiro
     * @var integer
     */
    private $account_nr = 0;

    /**
     * Your BankGiro number
     * @var integer
     */
    private $bankgiro_nr = 0;

    /**
     * Date when the salary is to be payed
     * @var DateTimeInterface
     */
    private $date;

    /**
     * What currency the salary is to be payed in
     * @var string
     */
    private $currency = 'SEK';

    /** 
     *
     * @param integer $account_nr Your account number at BankGiro
     * @param integer $bankgiro_nr Your BankGiro number
     * @param DateTimeInterface $date
     */
    public function __construct($account_nr, $bankgiro_nr, $date = null) {
        $this->account_nr = $account_nr;
        $this->bankgiro_nr = $bankgiro_nr;
        if ($date) {
            $this->setDate($date);
        }
    }

    /**
     * Set payment date
     *
     * @param DateTimeInterface $date
     * @return Salary
     */
    public function setDate(DateTimeInterface $date) {
        $this->date = $date;
        return $this;
    }

    /**
     * Return payment date
     *
     * @return DateTimeInterface
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set currency the payment is in
     *
     * @param string $currency Three letter currency
     */
    public function setCurrency($currency) {
        $this->currency = $currency;
    }

    /**
     * Add salary
     *
     *
     * @param integer $account Account number
     * @param float $pay Salary in float format
     * @param integer $employment_nr
     * @param string @msg Message to be sent with the payment max 12 charecter
     * @return Salary
     */
    public function add($account, $pay, $employment_nr, $msg='') {
        $this->rows[] = [
            'account'=>$account,
            'pay'=>$pay,
            'employment_nr'=>$employment_nr,
            'message'=>$msg
        ];
        return $this;
    }

    /**
     * Return a valid BankGiro Lön string
     *
     * @throws InvalidArgumentException on invalid date
     * @return string
     */
    public function format() {
        if (!($this->date instanceOf DateTimeInterface)) {
            throw new \InvalidArgumentException("Not a valid date");
        }
        $date = $this->date->format('ymd');
        $data = mb_convert_encoding(vsprintf($this->formats['TK01'], [
            $date, 
            $this->currency, 
            $this->account_nr, 
            $this->bankgiro_nr
        ]) , 'ISO-8859-1');
        $total = 0;
        foreach ($this->rows as $row) {
            $account = preg_replace("/[^0-9]/", "", $row['account']);
            $total += $row['pay'];
            $data .= vsprintf($this->formats['TK35'], [
                $date, 
                substr($account, 0, 4),
                substr($account, 4),
                round($row['pay']*100),
                $row['employment_nr'],
                mb_convert_encoding($row['message'], 'ISO-8859-1')
            ]);
        }
        $data .= vsprintf($this->formats['TK09'], [
            $date,
            round($total*100),
            count($this->rows)
        ]);
        return $data;
    }

    /**
     * Alias for Salary::format()
     *
     * @see Salary::format()
     * @return string
     */
    public function __toString() {
        return $this->format();
    }
}